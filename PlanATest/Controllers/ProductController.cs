﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PlanATest.Domain.IServices;
using PlanATest.Domain.Models;
using PlanATest.Domain.Models.Dto.Product;

namespace PlanATest.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiController]
    [Route("/[controller]/[action]")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _service;

        public ProductController(IProductService service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<IActionResult> Add(ProductFormDto dto)
        {
            var pro = new Product(dto.CategoryId,dto.Title,dto.Description,dto.Price,dto.Image,dto.DietaryFlagId);
            var product = await _service.Create(pro);
            if(product!=null)
                return Ok(product);
            return NotFound();
        }
        [HttpGet]
        public async Task<IActionResult> List([FromQuery] int pageSize = 5   , [FromQuery] int pageNumber = 0)
        {
            var list = await _service.GetAll(pageSize,pageNumber);
            return Ok(list);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> View(Guid id)
        {
            var product = await _service.View(id);
            return Ok(product);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var product =  _service.GetOne(id);
            if (product == null)
                return NotFound("PRODUCT NOT FOUND");
            await _service.Delete(product);
            return Ok(product);
        }

    }
}