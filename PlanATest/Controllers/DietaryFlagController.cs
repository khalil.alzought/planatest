﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PlanATest.Domain.IServices;
using PlanATest.Domain.Models;
using PlanATest.Domain.Models.Dto;
using PlanATest.Domain.Models.Dto.DietaryFlag;
using PlanATest.Domain.Models.Dto.Product;
using PlanATest.Domain.Repositories;
using PlanATest.Domain.Services;

namespace PlanATest.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiController]
    [Route("/[controller]/[action]")]
    public class DietaryFlagController : ControllerBase
    {
        private readonly IRepository<DietaryFlag> _repository;
        private readonly IMapper _mapper;

        public DietaryFlagController(IRepository<DietaryFlag> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> Add(DietaryFlagFormDto dto)
        {
            var dietaryFlag = new DietaryFlag(dto.Name, dto.Code);
            await _repository.CreateAsync(dietaryFlag);
            return Ok(dietaryFlag);
        }
        [HttpGet]
        public async Task<IActionResult> List()
        {
            var list = await _repository.GetListAsync(null);
            var data = list.ProjectTo<DietaryFlagViewDto>(_mapper.ConfigurationProvider);
            return Ok(data);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> View(int id)
        {
            var data =  _repository.FirstByConditionAsync(x => x.Id == id).ProjectTo<DietaryFlagViewDto>(_mapper.ConfigurationProvider).FirstOrDefault();
            if (data == null)
                return NotFound("DIETARY FLAG NOT FOUND");
            return Ok(data);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var dietaryFlag =  _repository.FirstByConditionAsync(x => x.Id == id).FirstOrDefault();
            if (dietaryFlag == null)
                return NotFound("DIETARY NOT FOUND");
            await _repository.Delete(dietaryFlag);
            return Ok(dietaryFlag);
        }

    }
}