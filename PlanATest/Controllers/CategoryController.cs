﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PlanATest.Domain.IServices;
using PlanATest.Domain.Models;
using PlanATest.Domain.Models.Dto.Category;

namespace PlanATest.Controllers
{

    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiController]
    [Route("/[controller]/[action]")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _service;

        public CategoryController(ICategoryService service)
        {
            _service = service;
        }


        [HttpPost]
        public async Task<IActionResult> Add(CategoryFormDto dto)
        {
            var cat = new Category(dto.Name, dto.ParentId);
            // await _service.Create(cat);
            return Ok(cat);
        }

        [HttpGet]
        public async Task<IActionResult> List([FromQuery] int pageSize =  5, [FromQuery] int pageNumber = 0)
        {
            var list = await _service.Find(pageSize , pageNumber);
            return Ok(list);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> View(Guid id)
        {
            var category = await _service.View(id);
            return Ok(category);
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var category =  _service.GetOne(id);
            if (category == null)
                return NotFound("CATEGORY NOT FOUND");
            await _service.Delete(category);
            return Ok(category);
        }

    }
}