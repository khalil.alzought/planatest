﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace PlanATest.Controllers.AuthControllers
{
    [ApiController]
    [Route("/[controller]/[action]")]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        public AccountController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }


        [HttpPost]
        public  async Task<IActionResult> Register(UserDto dto)
        {
            var identityUser = new IdentityUser(dto.Username);
            var result = await _userManager.CreateAsync(identityUser, dto.Password);
            if (result.Succeeded)
                return Ok(identityUser);
            return BadRequest();
        }

        [HttpPost]
        public async Task<IActionResult> Login(UserDto dto)
        {
            var identityUser = await _userManager.FindByNameAsync(dto.Username);
            if (identityUser != null &&  await _signInManager.CheckPasswordSignInAsync(identityUser, dto.Password, true) == SignInResult.Success)
            {
                return Ok(GenerateToken(identityUser.Id));
            }

            return BadRequest("Error In Username OR Password ");
        }

        private string GenerateToken(string userId)
        {
            var mySecret = "asdv234234^&%&^%&^hjsdfb2%%%";
            var mySecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(mySecret));
            var myIssuer = "http://mysite.com";
            var myAudience = "http://myaudience.com";
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, userId),
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                Issuer = myIssuer,
                Audience = myAudience,
                SigningCredentials = new SigningCredentials(mySecurityKey, SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }

    public class UserDto {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}