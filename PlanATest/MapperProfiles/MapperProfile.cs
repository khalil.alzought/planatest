﻿using AutoMapper;
using PlanATest.Domain.Models;
using PlanATest.Domain.Models.Dto.Category;
using PlanATest.Domain.Models.Dto.DietaryFlag;
using PlanATest.Domain.Models.Dto.Product;

namespace PlanATest.MapperProfiles
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Product, ProductViewDto>();
            CreateMap<Category, CategoryViewDto>();
            CreateMap<DietaryFlag,DietaryFlagViewDto>();
        }
    }
}