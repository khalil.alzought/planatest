﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using PlanATest.Domain.IServices;
using PlanATest.Domain.Models;
using PlanATest.Domain.Models.Dto.Category;
using PlanATest.Domain.Repositories;

namespace PlanATest.Domain.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IRepository<Category> _repository;
        private readonly IMapper _mapper;

        public CategoryService(IRepository<Category> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }


        public Task<Category> Create(Category category)
        {
            return _repository.CreateAsync(category);
        }

        public async Task<List<CategoryViewDto>> Find(int take, int skip)
        {
            var list =  await _repository.GetListAsync(null);
            return await list.ProjectTo<CategoryViewDto>(_mapper.ConfigurationProvider)
                .Take(take)
                .Skip(skip).ToListAsync();
        }

        public async Task<CategoryViewDto> View(Guid guid)
        {
            var query =  _repository.FirstByConditionAsync(pro => pro.Id == guid);
            var category =  _repository.FirstByConditionAsync(pro => pro.Id == guid).FirstOrDefault();
            if (category != null)
            {
                var data = query.ProjectTo<CategoryViewDto>(_mapper.ConfigurationProvider).FirstOrDefault();
                return data;
            }

            return null;
        }

        public Category GetOne(Guid Guid)
        {
            return  _repository.FirstByConditionAsync(pro => pro.Id == Guid).FirstOrDefault();

        }

        public async Task<bool> CheckExistingCategory(Guid CategoryId)
        {
            return _repository.FirstByConditionAsync(null).Any(x=>x.Id == CategoryId);
        }

        public  Task Delete(Category category)
        {
            return  _repository.Delete(category);
        }
    }
}