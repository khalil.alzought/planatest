﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using PlanATest.Domain.IServices;
using PlanATest.Domain.Models;
using PlanATest.Domain.Models.Dto.Product;
using PlanATest.Domain.Repositories;

namespace PlanATest.Domain.Services
{
    public class ProductService : IProductService
    {
        private readonly IRepository<Product> _repository;
        private readonly IMapper _mapper;
        private readonly ICategoryService _categoryService;
        private readonly IRepository<DietaryFlag> _dietaryFlagRepository;

        public ProductService(IRepository<Product> repository, IMapper mapper, ICategoryService categoryService, IRepository<DietaryFlag> dietaryFlagRepository)
        {
            _repository = repository;
            _mapper = mapper;
            _categoryService = categoryService;
            _dietaryFlagRepository = dietaryFlagRepository;
        }

        public async Task<List<ProductViewDto>> GetAll(int take  , int skip )
        {
            var list =  await _repository.GetListAsync(null);
            return await list.ProjectTo<ProductViewDto>(_mapper.ConfigurationProvider)
                .Take(take)
                .Skip(skip).ToListAsync();
        }

        public async Task<ProductViewDto> View(Guid guid)
        {

            var query =  _repository.FirstByConditionAsync(pro => pro.Id == guid );
            var product =  _repository.FirstByConditionAsync(pro => pro.Id == guid ).FirstOrDefault();
            if (product != null)
            {
                product.NumberOfViews++;
                await _repository.Update(product);
                var data = query.ProjectTo<ProductViewDto>(_mapper.ConfigurationProvider).FirstOrDefault();
                return data;
            }

            return null;
        }

        public Product GetOne(Guid Guid)
        {
            return _repository.FirstByConditionAsync(x => x.Id == Guid).FirstOrDefault();
            ;
        }

        public Task Delete(Product product)
        {
            return _repository.Delete(product);
        }

        public async Task<Product> Create(Product product)
        {
            var x= await _categoryService.CheckExistingCategory(product.CategoryId);
            if(x)
                return await _repository.CreateAsync(product);
            return null;
        }
          
    }
}