﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PlanATest.Domain.Repositories
{
    public interface IRepository<T> where T :  class
    {
        IQueryable<T> FirstByConditionAsync(Expression<Func<T, bool>> expression, bool asNoTracking = true);
        IQueryable<T> FirstByConditionAsync<TProperty>(Expression<Func<T, bool>> expression, bool asNoTracking = true,
            Expression<Func<T, TProperty>> navigationPropertyPath = null);
        Task<IQueryable<T>> GetListAsync(Expression<Func<T, bool>> expression,  bool asNoTracking = true  );
        Task<T> CreateAsync( T obj);
        Task<T> Update(T obj);
        Task SaveChangesAsync();
        Task Delete(T obj);
    }
}