﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PlanATest.Domain.Models
{
    public class Category
    {
        public Guid Id { get; private set; }
        public string Name { get; private set; }
        public Guid? ParentId { get; private set; }
        
        public Category Parent { get; set; }
        public ICollection<Category> InverseParent { get; set; }
        public ICollection<Product> Products { get; set; }

        public Category(string name, Guid? parentId)
        {
            Id = Guid.NewGuid();
            Name = name;
            ParentId = parentId;
        }
    }
}