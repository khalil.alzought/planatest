﻿using System;
using System.ComponentModel.DataAnnotations;
using PlanATest.Helpers;

namespace PlanATest.Domain.Models
{
    public class Product
    {
        public Guid Id { get; private set; }
        public Guid CategoryId { get; private set; }
        public string Title { get; private set; }
        [MaxLength(1000)]
        public string Description { get; set; }
        public int Price { get; set; }
        public string Image { get; set; }
        public int DietaryFlagId { get; set; } 
        public int NumberOfViews { get; set; }
        
        public virtual Category Category { get; set; }
        
        public virtual DietaryFlag DietaryFlag { get; set; }

        public Product(Guid categoryId, string title, string description, int price, string image, int dietaryFlagId)
        {
            Id = Guid.NewGuid();
            CategoryId = categoryId;
            Title = title;
            Description = description;
            Price = price;
            Image = image;
            DietaryFlagId = dietaryFlagId; 
            NumberOfViews = 0;
        }

     
    }
}