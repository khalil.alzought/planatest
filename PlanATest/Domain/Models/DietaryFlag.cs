﻿using System.Collections;
using System.Collections.Generic;

namespace PlanATest.Domain.Models
{
    public class DietaryFlag
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        
        public ICollection<Product> Products { get; set; }

        public DietaryFlag(string name, string code)
        {
            Name = name;
            Code = code;
        }
    }
}