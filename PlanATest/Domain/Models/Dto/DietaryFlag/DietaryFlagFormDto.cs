﻿namespace PlanATest.Domain.Models.Dto.DietaryFlag
{
    public class DietaryFlagFormDto
    {
        public string Name { get; set; }
        public string Code { get; set; }

    }
}