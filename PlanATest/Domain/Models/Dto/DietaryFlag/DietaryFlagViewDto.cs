﻿namespace PlanATest.Domain.Models.Dto.DietaryFlag
{
    public class DietaryFlagViewDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

    }
}