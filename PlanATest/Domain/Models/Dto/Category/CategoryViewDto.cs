﻿using System;

namespace PlanATest.Domain.Models.Dto.Category
{
    public class CategoryViewDto
    {
          public Guid Id { get; private set; }
          public string Name { get; private set; }
          public Guid? ParentId { get; private set; }
          public string InverseParentName { get; set; }
    }
}