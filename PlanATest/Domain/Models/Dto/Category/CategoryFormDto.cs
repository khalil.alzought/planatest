﻿using System;

namespace PlanATest.Domain.Models.Dto.Category
{
    public class CategoryFormDto
    {
        public string Name { get;  set; }
        public Guid? ParentId { get; set; }
    }
}