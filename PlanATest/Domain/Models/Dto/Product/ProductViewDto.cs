﻿using System;
using System.ComponentModel.DataAnnotations;
using PlanATest.Helpers;

namespace PlanATest.Domain.Models.Dto.Product
{
    public class ProductViewDto
    {
        public Guid Id { get;  set; }
        public Guid CategoryId { get;  set; }
        public string CategoryName { get; set; }
        public string Title { get; set; }
        [MaxLength(1000)]
        public string Description { get; set; }
        public int Price { get; set; }
        public string Image { get; set; }
        public int DietaryFlagId { get; set; } 
        public string DietaryFlagName { get; set; }
        public string DietaryFlagCode { get; set; }
        public int NumberOfViews { get; set; }

    }
}