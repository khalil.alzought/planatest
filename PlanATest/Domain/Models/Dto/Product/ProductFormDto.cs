﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PlanATest.Domain.Models.Dto.Product
{
    public class ProductFormDto
    {
        public Guid CategoryId { get;  set; }
        public string Title { get; set; }
        [MaxLength(1000)]
        public string Description { get; set; }
        public int Price { get; set; }
        public string Image { get; set; }
        public int DietaryFlagId { get; set; } 
    }
}