﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PlanATest.Domain.Models;
using PlanATest.Domain.Models.Dto.Product;
using PlanATest.Domain.Services;

namespace PlanATest.Domain.IServices
{
    public interface IProductService
    {
        public Task<Product> Create(Product product);
        public  Task<List<ProductViewDto>> GetAll(int take , int skip);
        
        public Task<ProductViewDto> View(Guid Guid);
        public Product GetOne(Guid Guid);

        public Task Delete(Product product);
    }
}