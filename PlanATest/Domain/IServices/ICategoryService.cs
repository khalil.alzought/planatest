﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PlanATest.Domain.Models;
using PlanATest.Domain.Models.Dto.Category;

namespace PlanATest.Domain.IServices
{
    public interface ICategoryService
    {
        
        public Task<Category> Create(Category category);
        public  Task<List<CategoryViewDto>> Find(int take , int skip);
        public Task<CategoryViewDto> View(Guid Guid);
        public Category GetOne(Guid Guid);
        public Task<bool> CheckExistingCategory(Guid CategoryId);
        public Task Delete(Category category);

    }
}