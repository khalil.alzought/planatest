﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PlanATest.Domain.Repositories;
using PlanATest.Infrastrcture.Persistance.Context;

namespace PlanATest.Infrastrcture.Repositories
{
    public class Repository<T>: IRepository<T> where T : class
    {
        private readonly AppDbContext _dbContext;

        public Repository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<T> FirstByConditionAsync(Expression<Func<T, bool>> expression, bool asNoTracking = true)
        {
            var query = _dbContext.Set<T>().AsQueryable();
            if (asNoTracking)
                query = query.AsNoTracking();

            if (expression != null)
                query = query.Where(expression);

            return  query;
        }

        public IQueryable<T> FirstByConditionAsync<TProperty>(Expression<Func<T, bool>> expression,
            bool asNoTracking = true,
            Expression<Func<T, TProperty>> navigationPropertyPath = null)
        {
             var query = _dbContext.Set<T>().AsQueryable();
                        if (asNoTracking)
                            query = query.AsNoTracking();
            
                        if (expression != null)
                            query = query.Where(expression);
            
                        if (navigationPropertyPath != null)
                        {
                            query = query.Include(navigationPropertyPath);
                        }
            
                        return  query;
        }

        public async Task<IQueryable<T>> GetListAsync(Expression<Func<T, bool>> expression, bool asNoTracking = true )
        {
            IQueryable<T> query = _dbContext.Set<T>();

            if (asNoTracking)
                query = query.AsNoTracking();

            if (expression != null)
                query = query.Where(expression);

            return   query;
        }

        public async Task<T> CreateAsync(T obj)
        {
            await _dbContext.AddAsync(obj);
            await _dbContext.SaveChangesAsync();
            return obj;
        }

        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        public async Task<T> Update(T obj)
        {
            _dbContext.Entry(obj).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
            return obj;
        }

        public async Task Delete(T obj)
        {
            _dbContext.Remove(obj);
            await _dbContext.SaveChangesAsync();
        }
        
    }
}