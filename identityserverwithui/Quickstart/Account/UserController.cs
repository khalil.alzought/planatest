﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace identityserverwithui.Quickstart.Account
{
    [ApiController]
    [Route("/[controller]/[action]")]
    public class UserController : ControllerBase
    {

        private readonly UserManager<IdentityUser> _userManager;
        private readonly ApplicationDbContext _context;

        public UserController(UserManager<IdentityUser> userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        [HttpPost]
        public async Task<IActionResult> Create(RegisterUserDto dto)
        {
            var identityUser = new IdentityUser(dto.Username);
            var x = await _userManager.CreateAsync(identityUser, dto.Password);
            return Ok(identityUser);
        }

    }

    public class RegisterUserDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}