import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.scss']
})
export class ViewProductComponent implements OnInit {

  product : any = {};
  param : any;
  constructor(private service:ProductService ,private activatedRouter : ActivatedRoute ) { }

  ngOnInit(): void {
    this.activatedRouter.params.subscribe(data => {
      this.param = data;
    });

 this.service.view(this.param.id).subscribe(data =>
  {
    this.product = data;
  });


  }

}
