import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss']
})
export class ListProductComponent implements OnInit {

  dataSource :any;
  
  displayedColumns: string[] = ['id', 'categoryId', 'title', 'description','price' , 'numberOfViews','commands'];

  constructor(private productservice : ProductService) { }

  ngOnInit(): void {
    this.productservice.listProduct().subscribe(data=>
      this.dataSource = data)
      
  }
  confirmDelete(product:any){
  var isConfiremd = confirm('are you sure you want to delete the product ' + product.title )
  this.productservice.delete(product.id).subscribe(data=>{

  },err=>{
    console.log(err)
  });

  }

}
