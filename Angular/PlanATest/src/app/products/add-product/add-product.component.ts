import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CategoryService } from 'src/app/services/category.service';
import { DeitaryflagService } from 'src/app/services/deitaryflag.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  product:any= {};
  //todo 
  categories :any ;
  DietaryFlags :any = [{id:"1223" , name :"hii"},{id:"1223" , name :"hii"},{id:"1223" , name :"hii"}];
  constructor(private service : ProductService , private categoryservice: CategoryService ,  private dietaryFlagService : DeitaryflagService,private _snackBar: MatSnackBar ) { }

  ngOnInit(): void {

    this.categoryservice.listCategory().subscribe(data=>{
      this.categories = data;
    })

    this.dietaryFlagService.listFlags().subscribe(data=>{
      this.DietaryFlags = data
    })
  }


  
  onSubmit(ngForm:any){
    
    this.service.add(ngForm.form.value).subscribe(data=>{
      this._snackBar.open("Category Created Successfully" , 'Done')
    },err=>{
      this._snackBar.open("Category Can't Created" , 'Fail')
    })
    
  }

}
