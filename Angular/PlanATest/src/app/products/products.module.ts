import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewProductComponent } from './view-product/view-product.component';
import { ListProductComponent } from './list-product/list-product.component';
import { AddProductComponent } from './add-product/add-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';

import {MatTableModule} from '@angular/material/table'; 
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import {MatSelectModule} from '@angular/material/select'; 
import {MatSnackBarModule} from '@angular/material/snack-bar'; 

@NgModule({
  declarations: [
    ViewProductComponent,
    ListProductComponent,
    AddProductComponent,
    EditProductComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    MatTableModule,
    MatSnackBarModule,
    MatSelectModule
  ],
  exports:[
    ViewProductComponent,
    ListProductComponent,
    AddProductComponent,
    EditProductComponent,
  ]
})
export class ProductsModule { }
