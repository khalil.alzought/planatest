import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from 'src/app/services/category.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {

  categories:any;
  product:any;
  param:any
  constructor(private service : ProductService ,private categoryService :CategoryService, private activatedRouter:ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRouter.params.subscribe(data => {
      this.param = data;
    });


    this.categoryService.listCategory().subscribe(data=>{
      this.categories = data
    })
    this.service.view(this.param.id).subscribe(data=>{
      this.product = data
    })

  }

}
