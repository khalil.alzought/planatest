import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  baseUrl = "https://localhost:5001/Category/"
  constructor(private client : HttpClient) {}

  listCategory()
  {
    return this.client.get(this.baseUrl + 'List');
  }

  delete(id:string)
  {
    return this.client.delete(this.baseUrl + 'Delete/'+id);
  }

  view(id:string)
  {
    return this.client.get(this.baseUrl + 'View/' + id);
  }
  add(obj:any)
  {
    return this.client.post<any>(this.baseUrl+'Add',obj);
  }
}
