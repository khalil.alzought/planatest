import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  baseUrl = "https://localhost:5001/Product/"
  constructor(private client : HttpClient) {}

  listProduct()
  {
    return this.client.get(this.baseUrl + 'List');
  }

  delete(id:string)
  {
    return this.client.delete(this.baseUrl + 'Delete/'+id);
  }

  view(id:string)
  {
    return this.client.get(this.baseUrl + 'View/' + id);
  }
  add(obj:any)
  {
    var formdata = new FormData();
    formdata.append('file' , obj.file);
    formdata.append('title' , obj.title);
    formdata.append('description' , obj.descriprion);
    formdata.append('price' , obj.price);
    formdata.append('categoryId' , obj.categoryId);
    formdata.append('deitaryFlagId' , obj.deitaryFlagId);

    return this.client.post<any>(this.baseUrl+'Add',obj);

  }
}
