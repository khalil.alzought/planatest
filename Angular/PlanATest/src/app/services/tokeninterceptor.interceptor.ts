import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class TokeninterceptorInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let tokenize = request.clone({
      setHeaders : {
        Authorization: 'Bearer '+ 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiI5YzUxZTViMy1kMDY2LTRiMGMtOTExOC1hMjEwNGZhYTU0MDYiLCJuYmYiOjE2NTY5NjM4MTcsImV4cCI6MTY1NzU2ODYxNywiaWF0IjoxNjU2OTYzODE3LCJpc3MiOiJodHRwOi8vbXlzaXRlLmNvbSIsImF1ZCI6Imh0dHA6Ly9teWF1ZGllbmNlLmNvbSJ9.yPI-cK9cqIEZrBcxbJG8xKb7UGGXQ5JJHhorrE2MaPE'
      }
    })
    return next.handle(tokenize);
  }
}
