import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddCategoryComponent } from './categories/add-category/add-category.component';
import { EditCategoryComponent } from './categories/edit-category/edit-category.component';
import { ListCategoryComponent } from './categories/list-category/list-category.component';
import { ViewCategoryComponent } from './categories/view-category/view-category.component';
import { AddDietaryFlagComponent } from './dietary-flags/add-dietary-flag/add-dietary-flag.component';
import { EditDietaryFlagComponent } from './dietary-flags/edit-dietary-flag/edit-dietary-flag.component';
import { ListDietaryFlagComponent } from './dietary-flags/list-dietary-flag/list-dietary-flag.component';
import { ViewDietaryFlagComponent } from './dietary-flags/view-dietary-flag/view-dietary-flag.component';
import { AddProductComponent } from './products/add-product/add-product.component';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { ListProductComponent } from './products/list-product/list-product.component';
import { ViewProductComponent } from './products/view-product/view-product.component';

const routes: Routes = [
  { path:'Product',
    children :[
      {path:'' , component: ListProductComponent},
      {path:'List' , component: ListProductComponent},
      {path:'Add' , component: AddProductComponent},
      {path:'View/:id' , component: ViewProductComponent},
      {path:'Edit/:id' , component: EditProductComponent},
    ]},
    { path:'Category',
    children :[
      {path:'' , component: ListCategoryComponent},
      {path:'List' , component: ListCategoryComponent},
      {path:'Add' , component: AddCategoryComponent},
      {path:'View/:id' , component: ViewCategoryComponent},
      {path:'Edit/:id' , component: EditCategoryComponent},
    ]},
    { path:'DietaryFlag',
    children :[
      {path:'' , component: ListDietaryFlagComponent},
      {path:'List' , component: ListDietaryFlagComponent},
      {path:'Add' , component: AddDietaryFlagComponent},
      {path:'View/:id' , component: ViewDietaryFlagComponent},
      {path:'Edit/:id' , component: EditDietaryFlagComponent},
    ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
