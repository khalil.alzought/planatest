import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from 'src/app/services/category.service';
import { CategoriesModule } from '../categories.module';

@Component({
  selector: 'app-view-category',
  templateUrl: './view-category.component.html',
  styleUrls: ['./view-category.component.scss']
})
export class ViewCategoryComponent implements OnInit {

  category:any;
  param:any;
  constructor(private service:CategoryService ,private activatedRouter : ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRouter.params.subscribe(data => {
      this.param = data;
    });

 this.service.view(this.param.id).subscribe(data =>
  {
    this.category = data;
  });

  }

}
