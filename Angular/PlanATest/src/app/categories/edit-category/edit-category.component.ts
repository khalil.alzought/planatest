import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit {

  categories :any;
  category:any;
  param:any
  constructor(private service : CategoryService , private activatedRouter:ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRouter.params.subscribe(data => {
      this.param = data;
    });

    this.service.listCategory().subscribe(data=>{
      this.categories = data
    })
    this.service.view(this.param.id).subscribe(data=>{
      this.category = data
    })
  }

}
