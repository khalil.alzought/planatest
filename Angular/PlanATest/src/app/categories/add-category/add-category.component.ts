import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {

  category :any = {};
  categories :any;
  constructor( private categoryservice: CategoryService , private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.categoryservice.listCategory().subscribe(data=>{
      this.categories = data;
    })
  }

  onSubmit(ngForm:any){
    this.categoryservice.add(ngForm.form.value).subscribe(res=>{
      this._snackBar.open("Category Created Successfully" , 'Done')
    },err=>{
      this._snackBar.open("Category Can't Created" , 'Fail')
    })
  }

}
