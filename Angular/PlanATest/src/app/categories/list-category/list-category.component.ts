import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss']
})
export class ListCategoryComponent implements OnInit {

  dataSource :any;
  
  displayedColumns: string[] = ['id', 'name','commands'];

  constructor(private categoryservice : CategoryService) { }

  ngOnInit(): void {
    this.categoryservice.listCategory().subscribe(data=>
      this.dataSource = data)
  }
  confirmDelete(category:any){
    var isConfiremd = confirm('are you sure you want to delete the Category ' + category.name )
    if(isConfiremd)
      this.categoryservice.delete(category.id);
  
    }
  
}
