import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';
import { LayoutModule } from './layout/layout.module';
import { ProductsModule } from './products/products.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { CategoriesModule } from './categories/categories.module';
import { DietaryFlagsModule } from './dietary-flags/dietary-flags.module';
import {TokeninterceptorInterceptor} from "./services/tokeninterceptor.interceptor";
// import {AuthModule, LogLevel} from "angular-auth-oidc-client";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatSidenavModule,
    LayoutModule,
    DietaryFlagsModule,
    ProductsModule,
    CategoriesModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule,
   /* AuthModule.forRoot({
      config: {
        authority: 'https://localhost:3001',
        redirectUrl: 'http://localhost:4200/',
        postLogoutRedirectUri: 'http://localhost:4200/',
        clientId: 'angular',
        scope: 'openid',
        responseType: 'code',
        silentRenew: true,
        useRefreshToken: true,
        logLevel: LogLevel.Debug,
      },
    }),*/
  ],
  providers: [{
    provide:HTTP_INTERCEPTORS,
    useClass:TokeninterceptorInterceptor,
    multi:true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
