import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DeitaryflagService } from 'src/app/services/deitaryflag.service';

@Component({
  selector: 'app-edit-dietary-flag',
  templateUrl: './edit-dietary-flag.component.html',
  styleUrls: ['./edit-dietary-flag.component.scss']
})
export class EditDietaryFlagComponent implements OnInit {

  dietaryFlag :any
  flageId:any
  constructor(private service : DeitaryflagService , private activatedRoute : ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((data:any)=>{
        this.flageId = data.id
    })
    this.service.view(this.flageId).subscribe(data=>{
      this.dietaryFlag = data
    })
  }

}
