import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDietaryFlagComponent } from './edit-dietary-flag.component';

describe('EditDietaryFlagComponent', () => {
  let component: EditDietaryFlagComponent;
  let fixture: ComponentFixture<EditDietaryFlagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditDietaryFlagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDietaryFlagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
