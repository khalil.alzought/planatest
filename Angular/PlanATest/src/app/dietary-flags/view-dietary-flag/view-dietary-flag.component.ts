import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DeitaryflagService } from 'src/app/services/deitaryflag.service';

@Component({
  selector: 'app-view-dietary-flag',
  templateUrl: './view-dietary-flag.component.html',
  styleUrls: ['./view-dietary-flag.component.scss']
})
export class ViewDietaryFlagComponent implements OnInit {
  dietaryFlag : any 
  flagId : any;
  constructor(private service : DeitaryflagService , private activatedRouter : ActivatedRoute) { }

  ngOnInit(): void {

    this.activatedRouter.params.subscribe((data:any)=>{
      this.flagId = data.id
    })

    this.service.view(this.flagId).subscribe(data=>{
      this.dietaryFlag = data
    } , err=>{
      console.log(err)
    })
  }

}
