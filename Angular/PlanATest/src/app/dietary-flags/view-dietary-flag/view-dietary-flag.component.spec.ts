import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDietaryFlagComponent } from './view-dietary-flag.component';

describe('ViewDietaryFlagComponent', () => {
  let component: ViewDietaryFlagComponent;
  let fixture: ComponentFixture<ViewDietaryFlagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewDietaryFlagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDietaryFlagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
