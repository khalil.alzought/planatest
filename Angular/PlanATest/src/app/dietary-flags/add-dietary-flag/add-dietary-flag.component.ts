import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DeitaryflagService } from 'src/app/services/deitaryflag.service';

@Component({
  selector: 'app-add-dietary-flag',
  templateUrl: './add-dietary-flag.component.html',
  styleUrls: ['./add-dietary-flag.component.scss']
})
export class AddDietaryFlagComponent implements OnInit {

  dietaryFlag:any = {};
  constructor(private service:DeitaryflagService , private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  onSubmit(ngForm:any){
    
    this.service.add(ngForm.form.value).subscribe((data:any)=>{
      this._snackBar.open("Dietary Flag Created Successfully" , 'Done')
    },(err:any)=>{
      this._snackBar.open("Dietary Flag Can't Created" , 'Fail')
    })
    
  }
}
