import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDietaryFlagComponent } from './add-dietary-flag.component';

describe('AddDietaryFlagComponent', () => {
  let component: AddDietaryFlagComponent;
  let fixture: ComponentFixture<AddDietaryFlagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDietaryFlagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDietaryFlagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
