import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDietaryFlagComponent } from './list-dietary-flag.component';

describe('ListDietaryFlagComponent', () => {
  let component: ListDietaryFlagComponent;
  let fixture: ComponentFixture<ListDietaryFlagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListDietaryFlagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDietaryFlagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
