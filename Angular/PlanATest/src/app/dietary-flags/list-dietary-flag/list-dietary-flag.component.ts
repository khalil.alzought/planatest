import { Component, OnInit } from '@angular/core';
import { DeitaryflagService } from 'src/app/services/deitaryflag.service';

@Component({
  selector: 'app-list-dietary-flag',
  templateUrl: './list-dietary-flag.component.html',
  styleUrls: ['./list-dietary-flag.component.scss']
})
export class ListDietaryFlagComponent implements OnInit {

  dataSource :any;
  
  displayedColumns: string[] = ['id', 'name', 'code','commands'];

  constructor(private service:DeitaryflagService) { }

  ngOnInit(): void {
    this.service.listFlags().subscribe((data : any)=>{
      this.dataSource = data
    })
  }


  confirmDelete(product:any){
    var isConfiremd = confirm('are you sure you want to delete the product ' + product.title )
    this.service.delete(product.id).subscribe(data=>{

    },err=>{
        console.log(err)
    });
  }

}
