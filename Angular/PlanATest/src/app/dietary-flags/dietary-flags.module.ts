import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatSnackBarModule} from '@angular/material/snack-bar'; 
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ListDietaryFlagComponent } from './list-dietary-flag/list-dietary-flag.component';
import { ViewDietaryFlagComponent } from './view-dietary-flag/view-dietary-flag.component';
import { EditDietaryFlagComponent } from './edit-dietary-flag/edit-dietary-flag.component';
import { AddDietaryFlagComponent } from './add-dietary-flag/add-dietary-flag.component';


@NgModule({
  declarations: [
    ListDietaryFlagComponent,
    ViewDietaryFlagComponent,
    EditDietaryFlagComponent,
    AddDietaryFlagComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    MatTableModule,
    MatSnackBarModule,
    MatSelectModule
  ],
  exports : [
    ListDietaryFlagComponent,
    ViewDietaryFlagComponent,
    EditDietaryFlagComponent,
    AddDietaryFlagComponent
  ]
})
export class DietaryFlagsModule { }
